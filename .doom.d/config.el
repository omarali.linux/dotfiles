;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

(setq user-full-name "omarali linux"
      user-mail-address "omarali.linux@gmail.com")

(setq doom-theme 'doom-one)
(setq display-line-numbers-type t)

;; change `org-directory'. It must be set before org loads!
(setq org-directory "/home/omarali/000_second_brain")

(use-package! md-roam ; load immediately, before org-roam
  :config
  (setq md-roam-file-extension-single "md")) 
(add-hook 'after-init-hook 'org-roam-mode)

(setq org-roam-directory "/home/omarali/000_second_brain")
(after! org-roam
        (map! :leader
            :prefix "n"
            :desc "org-roam" "l" #'org-roam
            :desc "org-roam-insert" "i" #'org-roam-insert
            :desc "org-roam-switch-to-buffer" "b" #'org-roam-switch-to-buffer
            :desc "org-roam-find-file" "f" #'org-roam-find-file
            :desc "org-roam-show-graph" "g" #'org-roam-show-graph
            :desc "org-roam-capture" "c" #'org-roam-capture)
  		(setq org-roam-db-location "/home/omarali/000_second_brain/org-roam/org-roam.db"
        	org-roam-graph-exclude-matcher "private"))

;;###########################################################
(use-package org-journal
      :bind
      ("C-c n j" . org-journal-new-entry)
      :custom
      (org-journal-dir "/home/omarali/000_second_brain/_daily")
      (org-journal-date-prefix "#+TITLE: ")
      (org-journal-file-format "%Y-%m-%d.org")
      (org-journal-date-format "%d %B %Y (%A)"))
    (setq org-journal-enable-agenda-integration t)

(setq org-roam-capture-ref-templates
    '(("r" "ref" plain (function org-roam-capture--get-point)
       "%?"
       :file-name "websites/${slug}"
       :head "#+TITLE: ${title}
#+ROAM_KEY: ${ref}
- source :: ${ref}"
       :unnarrowed t)))


(setq org-roam-capture-templates
	'(
		;; [ ]TODO: Auto make 'org-journal entry' or link to today's existing one
		;; [ ]TODO: Auto make index file or link to an existing one

		("j" "[Journal] Entry" plain (function org-roam--capture-get-point)
		 "%?"
		 :file-name "entry_${slug}_%<%Y%m%d%H%M%S>"
		 :head "#+TITLE: ${slug}
Tags:
Author: 
Source: [[file:_daily/%<%Y-%m-%d>.org][%<%d %B %Y (%A)>]]\n"
		 ;;{>CODE<} ;;Function: Make 'org-journal entry' file
		 :unnarrowed t 
		 )
	 ;;###################################################################      	

		("l" "[Note] Literature" plain (function org-roam--capture-get-point)
		 "%?"
		 :file-name "${title}_%<%Y%m%d%H%M%S-LIT>" ;; {choose index} -> index file linking
		 :head "#+TITLE: ${title} - LIT\n
Tags:
Author: 
" ;; {choose index} -> index file linking
		 :unnarrowed t)

	 ;;###################################################################      	
		("p" "[Note] Permenant" plain (function org-roam--capture-get-point)
		 "%?"
		 :file-name "${slug}-%<%Y%m>-PERM"
		 :head "#+TITLE: Permenant: ${title} - LIT\n"
		 :unnarrowed t)
		))

;;###################################################################################################################
(require 'company-org-roam)
    (use-package company-org-roam
      :when (featurep! :completion company)
      :after org-roam
      :config
  		(set-company-backend! 'org-roam-mode 'company-org-roam))
        (setq org-roam-file-extensions '("org"))

(use-package deft
      :after org
      :bind
      ("C-c n d" . deft)
      :custom
      (deft-recursive t)
      (deft-use-filter-string-for-filename t)
      (deft-default-extension "org")
      (deft-directory org-roam-directory))

;; Interactive Org Roam Server Graph
(require 'simple-httpd)
(setq httpd-root "/var/www")
(httpd-start)

(use-package org-roam-server
  :bind
  ("C-c n s" . org-roam-server-mode)
  :ensure t
  :load-path "~/Desktop/org-roam-server")